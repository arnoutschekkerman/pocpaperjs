// rectangle tool

pg.tools.registerTool({
	id: 'rectangle',
	name: 'Rectangle'
});

pg.tools.rectangle = function () {
	var tool;

	var options = {};

	var activateTool = function () {

		// get options from local storage if present
		options = pg.tools.getLocalOptions(options);

		tool = new Tool();
		var mouseDown;
		var path;
		var rect;

		tool.onMouseDown = function (event) {
			mouseDown = event.downPoint;
		};


		tool.onMouseDrag = function (event) {
			if (event.event.button > 0) return;  // only first mouse button

			rect = new Rectangle(event.downPoint, event.point);

			if (event.modifiers.shift) {
				rect.height = rect.width;
			}

			path = new Path.Rectangle(rect);

			if (event.modifiers.alt) {
				path.position = mouseDown;
			}

			path = pg.stylebar.applyActiveToolbarStyle(path);

			// Remove this path on the next drag event:
			path.removeOnDrag();
		};

		tool.onMouseUp = function (event) {
			if (event.event.button > 0) return;  // only first mouse button

			pg.undo.snapshot('rectangle');
		};

		tool.activate();
	};

	var deactivateTool = function () {
		tool.remove();
	};

	return {
		options: options,
		activateTool: activateTool,
		deactivateTool: deactivateTool
	};
};


