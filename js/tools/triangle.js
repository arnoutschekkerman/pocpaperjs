// triangle tool

pg.tools.registerTool({
	id: 'triangle',
	name: 'Triangle'
});

pg.tools.triangle = function () {
	var tool;

	var options = {};

	var activateTool = function () {

		tool = new Tool();
		var mouseDown;
		var path;
		var triangle;

		tool.onMouseDown = function (event) {
			mouseDown = event.downPoint;
		};


		tool.onMouseDrag = function (event) {
			if (event.event.button > 0) return;  // only first mouse button

			// triangle = new Rectangle(event.downPoint, event.point);

			if (event.modifiers.shift) {
				pointA = [event.downPoint.x, event.downPoint.y];
				pointB = [event.downPoint.x, event.point.y];
				deltaYAB = Math.abs(event.downPoint.y - event.point.y);
				if (event.downPoint.x < event.point.x) {
					pointC = [event.downPoint.x + deltaYAB, event.point.y];
				} else {
					pointC = [event.downPoint.x - deltaYAB, event.point.y];
				}
			} else {
				pointA = [event.downPoint.x, event.downPoint.y];
				pointB = [event.downPoint.x, event.point.y];
				pointC = [event.point.x, event.point.y];
			}


			if (!path) {
				path = new Path({
					segments: [pointA, pointB, pointC],
					closed: true
				});
			} else {
				path.segments = [pointA, pointB, pointC]
			}

			if (event.modifiers.alt) {
				path.position = mouseDown;
			}

			path = pg.stylebar.applyActiveToolbarStyle(path);
			// Remove this path on the next drag event:
		};

		tool.onMouseUp = function (event) {
			if (event.event.button > 0) return;  // only first mouse button
			path = null;

			pg.undo.snapshot('triangle');
		};

		tool.activate();
	};

	var deactivateTool = function () {
		tool.remove();
	};

	return {
		options: options,
		activateTool: activateTool,
		deactivateTool: deactivateTool
	};
};


